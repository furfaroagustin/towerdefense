using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GeneradorMapa : MonoBehaviour
{
    //variables del mapa: tama�o
    [SerializeField]private int mapW;
    [SerializeField]private int mapH;
    public GameObject mapT;

    private List<GameObject> mapTiles = new List<GameObject>();
    private List<GameObject> mapPaths = new List<GameObject>();

    
    private bool reachedx=false;
    private bool reachedy = false;

    private GameObject CurrentTile;
    private int currentIndex;
    private int nextIndex;
    public Color pathColor;
    private List<GameObject> obtenerBordeSuperior() //GetTopEdgeTiles
    //edgeTile:cuadrado borde
    {
        List<GameObject> cuadradoBorde = new List<GameObject>();
        for (int i=mapW*(mapH-1);i<mapW*mapH;i++)
        {
            cuadradoBorde.Add(mapTiles[i]);
        }
        return cuadradoBorde;
    }
    private List<GameObject> obtenerBordeInferior() //GetBottomEdgeTiles
    {
        List<GameObject> cuadradoBorde = new List<GameObject>();
        for(int i=0;i<mapW;i++)
        {
            cuadradoBorde.Add(mapTiles[i]);
        }
        return cuadradoBorde;
    }
    private void MoveDown()
    {
        mapPaths.Add(CurrentTile);
        currentIndex = mapTiles.IndexOf(CurrentTile);
        nextIndex = currentIndex-mapW;
        CurrentTile = mapTiles[nextIndex];
    }
    private void MoveLeft()
    {
        mapPaths.Add(CurrentTile);
        currentIndex = mapTiles.IndexOf(CurrentTile);
        nextIndex= currentIndex-1;
        CurrentTile = mapTiles[nextIndex];
    }
    private void MoveRight()
    {
        mapPaths.Add(CurrentTile);
        currentIndex = mapTiles.IndexOf(CurrentTile);
        nextIndex = currentIndex+1;
        CurrentTile = mapTiles[nextIndex];
    }


    private void Start()
    {
        generarMapa();
    }
    private void generarMapa()
    {
        for(int y=0; y <mapH;y++)
        {
            for(int x=0;x<mapW;x++)
            {
                
                GameObject newM = Instantiate(mapT);
                mapTiles.Add(newM);
                newM.transform.position = new Vector2(x, y);

            }
        }
        List<GameObject> ObordeSuperior = obtenerBordeSuperior();
        List<GameObject> ObordeInferior = obtenerBordeInferior();
        //Destroy(mapTiles[0]);
        GameObject primerCuadrado;//StartTile
        GameObject ultimoCuadrado;//EndTile
        int rand1 = Random.Range(0, mapW);
        int rand2 = Random.Range(0, mapW);

        primerCuadrado = ObordeSuperior[rand1];
        ultimoCuadrado = ObordeInferior[rand2];
        CurrentTile = primerCuadrado;
        MoveDown();
        int loop=0;
        while(reachedx==false)
        {
            loop++;
            if(loop>100)
            {
                Debug.Log("SE PUDRIO TODO");
                break;
            }
            if (CurrentTile.transform.position.x > ultimoCuadrado.transform.position.x)
            {
                MoveLeft();

            }
            else if (CurrentTile.transform.position.x < ultimoCuadrado.transform.position.x) 
            {
                MoveRight();
            }
         
        }
        while (reachedy == false)
        {
           if (CurrentTile.transform.position.y > ultimoCuadrado.transform.position.y)
           {
               MoveDown();
           }
           else
           {
               reachedy = true;
           }
        }
        mapPaths.Add(ultimoCuadrado);
        foreach (GameObject obj in mapPaths)
        {
           obj.GetComponent<SpriteRenderer>().color = pathColor;
        }

    }

}
