using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour
{
    [SerializeField]
    private float enemyHealt;
    [SerializeField]
    private float movementSpeed;

    private int killRoward;//the amout of money the player gets when this enemy
    private int damage;// the amount of damage the enemy does when it reaches the end

    private GameObject targetTile;
    void Start()
    {
        initializeEnemy();
    }
    private void Awake()
    {
        Enemies.enemies.Add(gameObject);
    }
    private void initializeEnemy()
    {
        targetTile = MapGenerador.startTile;

    }  
    public void TakeDamage(float amount)
    {
        enemyHealt -= amount;
        if(enemyHealt<=0)
        {
            Die();
        }

    }
    private void Die()
    {
        Enemies.enemies.Remove(gameObject);
        Destroy(transform.gameObject);
    }
    private void moveEnemy()
    {
        transform.position = Vector3.MoveTowards(transform.position, targetTile.transform.position, movementSpeed * Time.deltaTime);
    }
    private void checkPosition()
    {
        if(targetTile!=null&&targetTile!=MapGenerador.endTile)
        {
            float distance = (transform.position - targetTile.transform.position).magnitude;
            if(distance<0.001f)
            {
                int currentIndex = MapGenerador.pathTiles.IndexOf(targetTile);
                targetTile = MapGenerador.pathTiles[currentIndex + 1];
            }
        }

    }
  
    void Update()
    {
        checkPosition();
        moveEnemy();
    }
}
